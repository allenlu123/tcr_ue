package com.techm.tcr_ue.main;

/**
 * Created by Allen on 7/30/2015.
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.techm.tcr_ue.R;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class WebActivity extends AppCompatActivity {
    Button b1;
    EditText ed1;

    private WebView wv1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1=(Button)findViewById(R.id.button);
        ed1=(EditText)findViewById(R.id.editText);

        wv1=(WebView)findViewById(R.id.webView);
        wv1.setWebViewClient(new MyBrowser());

        ed1.setOnTouchListener(new View.OnTouchListener() {
                                   @Override
                                   public boolean onTouch(View v, MotionEvent event) {
                                       InputMethodManager keyboard = (InputMethodManager)
                                               getSystemService(Context.INPUT_METHOD_SERVICE);
                                       keyboard.showSoftInput(ed1, 0);
                                       return false;
                                   }
                               }
        );

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = ed1.getText().toString();
                if (!(url.equals(null)) && !(url.equals(""))) {
                    if(url.length()>7){
                    if (!url.substring(0, 7).equals("http://")) {
                        url = "http://" + url;
                    }
                    }
                    else{
                        url = "http://" + url;
                    }

                    wv1.getSettings().setLoadsImagesAutomatically(true);
                    wv1.getSettings().setJavaScriptEnabled(true);
                    wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                    keyboard();
                    wv1.loadUrl(url);
                }
            }
        });

    }

    private void keyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.web, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.home_action:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
}