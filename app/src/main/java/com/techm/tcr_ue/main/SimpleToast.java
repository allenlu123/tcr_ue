package com.techm.tcr_ue.main;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Allen on 7/20/2015.
 */
public class SimpleToast {
    private static Toast mToast;

    public static void show(Context context, String text, int duration) {
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(context, text, duration);
        mToast.show();
    }
}
