package com.techm.tcr_ue.main;

/**
 * Created by rs0c48247 on 1/7/2015.
 */

   import android.content.BroadcastReceiver;
   import android.content.Context;
   import android.content.Intent;
   import android.os.Bundle;
   import android.telephony.SmsMessage;
   import android.widget.Toast;

public class SMSReceiver extends BroadcastReceiver {
    /* package */ static final String ACTION =
            "android.provider.Telephony.SMS_RECEIVED";
    private static final String LOG_TAG = "SMSReceiver";
    private SimpleToast st = new SimpleToast();

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ACTION)) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Object[] pdus = (Object[]) bundle.get("pdus");
                for (int i = 0; i < pdus.length; i++) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();

                    st.show(context, "SMS Message From " + senderNum + ":\n" + message,
                            Toast.LENGTH_LONG);
                }

            }
        }
    }


}
