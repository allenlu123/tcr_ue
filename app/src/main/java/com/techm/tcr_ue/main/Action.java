package com.techm.tcr_ue.main;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by rs0c48247 on 1/7/2015.
 */
public class Action {
    Context context;
    private SimpleToast st = new SimpleToast();
    public Action(Context context){
        this.context = context;
    }

    public void sendSMS(String phoneNo, String message){
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";
        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0,
                new Intent(SENT), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0,
                new Intent(DELIVERED), 0);
        //---when the SMS has been sent---
        context.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        st.show(context, "SMS Sent",
                                Toast.LENGTH_SHORT);
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        st.show(context, "Generic Failure",
                                Toast.LENGTH_SHORT);
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        st.show(context, "No Service",
                                Toast.LENGTH_SHORT);
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        st.show(context, "Null PDU",
                                Toast.LENGTH_SHORT);
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        st.show(context, "Radio Off",
                                Toast.LENGTH_SHORT);
                        break;
                }
            }
        }, new IntentFilter(SENT));
        //---when the SMS has been delivered---
        context.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        st.show(context, "SMS Delivered",
                                Toast.LENGTH_SHORT);
                        break;
                    case Activity.RESULT_CANCELED:
                        st.show(context, "SMS Not Delivered",
                                Toast.LENGTH_SHORT);
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(phoneNo, null, message, sentPI, deliveredPI);
    }
    public void sendVoice(String phoneNo, Activity activity){
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phoneNo));
        activity.startActivity(callIntent);
        st.show(context, "Voice Call Initiated.",
                Toast.LENGTH_SHORT);
    }

    public String getIMSI(Activity activity){
        TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        String subscriberId =  telephonyManager.getSubscriberId().toString();
        st.show(context, "Subscriber ID Is "+subscriberId,
                Toast.LENGTH_SHORT);
        return subscriberId;
    }

    //Can only use on Android Version 4.1 and below
   public boolean airplane() {
       boolean isEnabled = (Settings.System.getInt(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) == 1);
       Settings.System.putInt(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, isEnabled ? 0 : 1);
       Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
       intent.putExtra("state", !isEnabled);
       context.sendBroadcast(intent);
       isEnabled=!isEnabled;
       if(isEnabled){
           st.show(context, "Airplane Mode Now On", Toast.LENGTH_SHORT);
       }
       else{
           st.show(context, "Airplane Mode Now Off", Toast.LENGTH_SHORT);
       }
       return isEnabled;
   }
    public boolean isMobileDataEnabledFromLollipop() {
        boolean state;
            state = Settings.Global.getInt(context.getContentResolver(), "mobile_data", 0) == 1;
        return state;
    }
    public void setMobileNetworkfromLollipop() throws Exception {
        String command = null;
        String enabledisable="disable";
        boolean isconnect=isMobileDataEnabledFromLollipop();
        try {
            // Get the current state of the mobile network.
            // Get the value of the "TRANSACTION_setDataEnabled" field.
            String transactionCode = getTransactionCode(context);
            if (transactionCode != null && transactionCode.length() > 0) {
                // Execute the command via `su` to turn off mobile network.
                if(isconnect){
                    enabledisable="disable";
                }
                else if(!isconnect){
                    enabledisable="enable";
                }
                command = "svc data "+enabledisable;
                executeCommandViaSu(context, "-c", command);
            }
    } catch(Exception e) {
        // Oops! Something went wrong, so we throw the exception here.
        throw e;
    }
}
    private static String getTransactionCode(Context context) throws Exception {
        try {
            final TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            final Class<?> mTelephonyClass = Class.forName(mTelephonyManager.getClass().getName());
            final Method mTelephonyMethod = mTelephonyClass.getDeclaredMethod("getITelephony");
            mTelephonyMethod.setAccessible(true);
            final Object mTelephonyStub = mTelephonyMethod.invoke(mTelephonyManager);
            final Class<?> mTelephonyStubClass = Class.forName(mTelephonyStub.getClass().getName());
            final Class<?> mClass = mTelephonyStubClass.getDeclaringClass();
            final Field field = mClass.getDeclaredField("TRANSACTION_setDataEnabled");
            field.setAccessible(true);
            return String.valueOf(field.getInt(null));
        } catch (Exception e) {
            // The "TRANSACTION_setDataEnabled" field is not available,
            // or named differently in the current API level, so we throw
            // an exception and inform users that the method is not available.
            throw e;
        }
    }
    private static void executeCommandViaSu(Context context, String option, String command) {
        boolean success = false;
        String su = "su";
        for (int i=0; i < 3; i++) {
            // Default "su" command executed successfully, then quit.
            if (success) {
                break;
            }
            // Else, execute other "su" commands.
            if (i == 1) {
                su = "/system/xbin/su";
            } else if (i == 2) {
                su = "/system/bin/su";
            }
            try {
                // Execute command as "su".
                Runtime.getRuntime().exec(new String[]{su, option, command});
            } catch (IOException e) {
                success = false;
                // Oops! Cannot execute `su` for some reason.
                // Log error here.
            } finally {
                success = true;
            }
        }
    }
}
