/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.techm.tcr_ue.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.techm.tcr_ue.R;
import com.techm.tcr_ue.common.logger.Log;
import com.techm.tcr_ue.common.logger.LogFragment;
import com.techm.tcr_ue.common.logger.LogWrapper;
import com.techm.tcr_ue.common.logger.MessageOnlyLogFilter;
import com.techm.tcr_ue.util.SimpleTextFragment;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Sample application demonstrating how to connect to the network and fetch raw
 * HTML. It uses AsyncTask to do the fetch on a background thread. To establish
 * the network connection, it uses HttpURLConnection.
 *
 * This sample uses the logging framework to display log output in the log
 * fragment (LogFragment).
 */
public class MainActivity extends AppCompatActivity {
    public static final String TAG = "Network Connect";
    public ResultString testCase = new ResultString();
    /* Original URL that was here, did not work. Use a localhost with tomcat, ipconfig on cmd to get IPv4 address
    private String url = "http://192.168.213.114:8080/tcr_ws/testCase.htm";
     */

    //URL that I set up and used, from my home host computer
    private String url = "http://24.16.140.102:80/tcr_ws/testCase.htm";
    // Reference to the fragment showing events, so we can clear it with a button
    // as necessary.
    private LogFragment mLogFragment;
    private static int numberOfSMSSent = 1;
    private boolean repeat=true;
    private boolean start=true;
    private Runnable runnable;
    private Handler hand = new Handler();
    private SimpleToast st= new SimpleToast();
    private String log;
    private boolean smssend=true;
    private boolean smsreceive=true;
    private boolean voicecall=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tcr_ue_main);
        // Initialize text fragment that displays intro text.
        SimpleTextFragment introFragment = (SimpleTextFragment)
                getSupportFragmentManager().findFragmentById(R.id.intro_fragment);
        introFragment.setText(R.string.welcome_message);
        introFragment.getTextView().setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16.0f);

        // Initialize the logging framework.
        initializeLogging();

        TelephonyManager TelephonyMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        TelephonyMgr.listen(new TeleListener(this),
                PhoneStateListener.LISTEN_CALL_STATE);
        log="";

    }

    /*This is what initiates the action for starting the server
    connection process
     */
    @Override
    public void onResume() {
        super.onResume();
        subscriberId = new Action(getApplicationContext()).getIMSI(this);
        if(repeat) {
            initiateAction();
        }
    }
    public void initiateAction(){
            runnable = new Runnable() {
                @Override
                public void run() {
                    downloadTaskExecute();
                }
            };
            hand.postDelayed(runnable, 5000);
    }
    private String subscriberId;
    public void downloadTaskExecute(){
        hand.removeCallbacks(runnable);
        if(start) {
            new DownloadTask(this).execute(url + "?imsi=" + subscriberId);
        }
        else{
            hand.postDelayed(runnable,1000);
        }
    }

    @Override
    protected void onPause(){
        super.onPause();
        repeat=true;
    }



    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        System.out.println("Menu Created");
        super.onCreateOptionsMenu(menu);
        return true;
    }


   @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.fetch_action:
                start = !start;
                if(start) {
                    Log.i(TAG, "Application Resumed");
                    item.setIcon(R.drawable.ic_pause_white_24dp);
                }
                else{
                    Log.i(TAG, "Application Paused");
                    item.setIcon(R.drawable.ic_play_arrow_white_24dp);
                }
                return true;
            case R.id.clear_action:
                mLogFragment.getLogView().setText("");
                voicecall=true;
                smssend=true;
                smsreceive=true;
                return true;
            case R.id.mobile_data:
                try {
                    new Action(getApplicationContext()).setMobileNetworkfromLollipop();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                runnable = new Runnable() {
                    @Override
                    public void run() {
                    if(new Action(getApplicationContext()).isMobileDataEnabledFromLollipop())
                    {
                        Log.i(TAG, "Mobile Data Connected. Application Resumed.");
                        start=true;
                        initiateAction();
                    }
                    else
                    {
                        Log.i(TAG, "Mobile Data Disconnected. Application Paused Due To Data Disconnect.");
                        start=false;
                        initiateAction();
                    }
                }
        };
                hand.postDelayed(runnable, 1600);
                return true;

            case R.id.web_action:
            Intent myintent = new Intent(this,WebActivity.class);
                startActivity(myintent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

   }



    /**
     * Implementation of AsyncTask, to fetch the data in the background away from
     * the UI thread.
     */
    private class DownloadTask extends AsyncTask<String, Void, String> {
        private Activity activity;
        public DownloadTask(Activity activity) {
            this.activity = activity;
        }

        private Action action;

        @Override
        protected String doInBackground(String... urls) {
            try {
                return loadFromNetwork(urls[0]);
            } catch (IOException e) {
                return getString(R.string.connection_error);
            }
        }

        /**
         * Uses the logging framework to display the output of the fetch
         * operation in the log fragment.
         */


        @Override
        protected void onPostExecute(String result) {
            testCase.setResult(result);
            try {
                JSONArray jsonResponse = new JSONArray(testCase.getResult());
                int count = 0;
                String to_mobile_no = null;
                String action_case = null;
                while (count < jsonResponse.length()) {
                    if (subscriberId.equals(jsonResponse.getJSONObject(count).getString("IMSI"))) {
                        to_mobile_no = jsonResponse.getJSONObject(count).getString("TO_UE");
                        action_case = jsonResponse.getJSONObject(count).getString("CASE");
                        break;
                    }
                    count++;
                }
                if (count < jsonResponse.length()) {
                    action = new Action(getApplicationContext());
                    if (action_case.equals("SMS")) {
                        action.sendSMS(to_mobile_no, "SMS " + (numberOfSMSSent++) + " This is a DEMO Test Message from UE A to UE B");
                        if(smssend) {
                            log = "Application In SMS Sending Mode";
                            smssend=false;
                            smsreceive=true;
                            voicecall=true;
                        }
                    } else if (action_case.equals("VOICE")) {
                        repeat = false;
                        action.sendVoice(to_mobile_no, activity);
                        if(voicecall) {
                            log = "Application In Voice Call Mode";
                            smssend=true;
                            smsreceive=true;
                            voicecall=false;
                        }
                    }
                    else if(action_case.equals("RECEIVE")){
                        Intent intent = new Intent(getApplicationContext(),SMSReceiver.class);
                        intent.setAction("android.provider.Telephony.SMS_RECEIVED");
                        sendBroadcast(intent);
                        if(smsreceive) {
                            log = "Application In SMS Receiving Mode";
                            smssend=true;
                            smsreceive=false;
                            voicecall=true;
                        }
                    }

                } else {
                    repeat = true;
                    log = "IMSI Does Not Match Any Of The Options";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (log.equals("")) {
                log = "Connection error.";
                Log.i(TAG, log);
                log="Stop";
            }
            else if(!log.equals("Stop")){
                Log.i(TAG, log);
                log="Stop";
            }
            if (repeat) {
                    runnable = new Runnable() {
                        @Override
                        public void run() {
                            initiateAction();
                        }
                    };
                    hand.postDelayed(runnable, 10000);
                }

        }
    }
    private class TeleListener extends PhoneStateListener {
        private Activity activity;
        public TeleListener(Activity activity){
            this.activity=activity;
        }
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);
            switch (state) {
                case TelephonyManager.CALL_STATE_IDLE:
                    // CALL_STATE_IDLE;
                    st.show(getApplicationContext(), "CALL_STATE_IDLE",
                            Toast.LENGTH_SHORT);
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    // CALL_STATE_OFFHOOK;
                    st.show(getApplicationContext(), "CALL_STATE_OFFHOOK",
                            Toast.LENGTH_SHORT);
                    break;
                case TelephonyManager.CALL_STATE_RINGING:
                    // CALL_STATE_RINGING
                    st.show(getApplicationContext(), "CALL_STATE_RINGING "+incomingNumber,
                            Toast.LENGTH_SHORT);
                    break;
                default:
                    break;
            }
        }


    }

    /** Initiates the fetch operation. */
    private String loadFromNetwork(String urlString) throws IOException {
        InputStream stream = null;
        String str ="";

        try {
            stream = downloadUrl(urlString);
            str = readIt(stream, 500);
        } finally {
            if (stream != null){
                stream.close();
            }
        }
        return str;
    }



    /**
     * Given a string representation of a URL, sets up a connection and gets
     * an input stream.
     * @param urlString A string representation of a URL.
     * @return An InputStream retrieved from a successful HttpURLConnection.
     * @throws java.io.IOException
     */
    private InputStream downloadUrl(String urlString) throws IOException {
        // BEGIN_INCLUDE(get_inputstream)
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // Start the query
        conn.connect();
        InputStream stream = conn.getInputStream();
        return stream;
        // END_INCLUDE(get_inputstream)
    }

    /** Reads an InputStream and converts it to a String.
     * @param stream InputStream containing HTML from targeted site.
     * @param len Length of string that this method returns.
     * @return String concatenated according to len parameter.
     * @throws java.io.IOException
     * @throws java.io.UnsupportedEncodingException
     */
    private String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }

    /** Create a chain of targets that will receive log data */
    public void initializeLogging() {

        // Using Log, front-end to the logging chain, emulates
        // android.util.log method signatures.

        // Wraps Android's native log framework
        LogWrapper logWrapper = new LogWrapper();
        Log.setLogNode(logWrapper);

        // A filter that strips out everything except the message text.
        MessageOnlyLogFilter msgFilter = new MessageOnlyLogFilter();
        logWrapper.setNext(msgFilter);

        // On screen logging via a fragment with a TextView.
        mLogFragment =
                (LogFragment) getSupportFragmentManager().findFragmentById(R.id.log_fragment);
        msgFilter.setNext(mLogFragment.getLogView());
    }



}
