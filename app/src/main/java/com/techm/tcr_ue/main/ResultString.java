package com.techm.tcr_ue.main;

/**
 * Created by rs0c48247 on 1/9/2015.
 */
public class ResultString {
    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
